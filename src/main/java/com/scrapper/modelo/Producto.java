package com.scrapper.modelo;


import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
//import lombok.Data;
//@Data 
public class Producto {
    private int id;
    private float shipping;
    private String ASIN;
    private String titulo="";
    private String descripcion="";
    private float precio;
    private float precioML;
    private String categoria;
    private String subcategoria;
    private String marca="";
    private String modelo="";
    private float reviews=0.0f;
    private String status="ACTIVO";
    private String statusml="INACTIVO";
    private String url;
    private String disponible="";
    private String proveedor;
    private Timestamp fechaUpdate;
    private List<String> imagenes;
    private List<String> bullets;
    private Map<String,String> getDetallesBullet;
    private Map<String,String> getInformacionTecnica; 
    private String sku;
    private String mlCode;
    private boolean changed;
    
    
    @Override
    public String toString(){
        return "[PRODUCTO="+ASIN+"]="+titulo+"-"+precio+"-"+status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getShipping() {
        return shipping;
    }

    public void setShipping(float shipping) {
        this.shipping = shipping;
    }

    public String getASIN() {
        return ASIN;
    }

    public void setASIN(String ASIN) {
        this.ASIN = ASIN;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPrecioML() {
        return precioML;
    }

    public void setPrecioML(float precioML) {
        this.precioML = precioML;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getSubcategoria() {
        return subcategoria;
    }

    public void setSubcategoria(String subcategoria) {
        this.subcategoria = subcategoria;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public float getReviews() {
        return reviews;
    }

    public void setReviews(float reviews) {
        this.reviews = reviews;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusml() {
        return statusml;
    }

    public void setStatusml(String statusml) {
        this.statusml = statusml;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDisponible() {
        return disponible;
    }

    public void setDisponible(String disponible) {
        this.disponible = disponible;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public Timestamp getFechaUpdate() {
        return fechaUpdate;
    }

    public void setFechaUpdate(Timestamp fechaUpdate) {
        this.fechaUpdate = fechaUpdate;
    }

    public List<String> getImagenes() {
        return imagenes;
    }

    public void setImagenes(List<String> imagenes) {
        this.imagenes = imagenes;
    }

    public List<String> getBullets() {
        return bullets;
    }

    public void setBullets(List<String> bullets) {
        this.bullets = bullets;
    }

    public Map<String, String> getGetDetallesBullet() {
        return getDetallesBullet;
    }

    public void setGetDetallesBullet(Map<String, String> getDetallesBullet) {
        this.getDetallesBullet = getDetallesBullet;
    }

    public Map<String, String> getGetInformacionTecnica() {
        return getInformacionTecnica;
    }

    public void setGetInformacionTecnica(Map<String, String> getInformacionTecnica) {
        this.getInformacionTecnica = getInformacionTecnica;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getMlCode() {
        return mlCode;
    }

    public void setMlCode(String mlCode) {
        this.mlCode = mlCode;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }
    
    
}
