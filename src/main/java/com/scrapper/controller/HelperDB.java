package com.scrapper.controller;

import com.scrapper.db.MySQLConnectionFactory;
import com.scrapper.modelo.Producto;
import java.util.logging.Logger;

/**
 *
 * @author casa
 */
public class HelperDB {

    Producto producto = null;
    final MySQLConnectionFactory connectDB = new MySQLConnectionFactory();
    final Logger logger = Logger.getLogger(HelperDB.class.getName());

    public HelperDB(final Producto prod) throws Exception {
        this.producto = prod;

        try {
            System.out.println("=======================================================================");
            logger.info("Iniciando guardado en Base de Datos Producto " + producto.getASIN());
            connectDB.addProducto(producto);
            for (String imagen : producto.getImagenes()) {
                connectDB.addImagenes(producto.getASIN(), imagen);
            }
            for (String bullet : producto.getBullets()) {
                connectDB.addBullets(producto.getASIN(), bullet);
            }
            producto.getGetInformacionTecnica().forEach((k, v) -> connectDB.addAtributos(producto.getASIN(), k, v));
            producto.getGetDetallesBullet().forEach((k, v) -> connectDB.addAtributos(producto.getASIN(), k, v));
            logger.info("Finaliza el gaurdado del Producto" + producto.getASIN());
            System.out.println("=======================================================================");
        } catch (Exception e) {
            // Throwing an exception 
            logger.info("Error Guardando en BD ASIN= " + prod.getASIN());
            e.printStackTrace();
            throw new Exception("Producto no se pudo guardar en la tabla productos: " + e.getMessage());
        }
    }

}
