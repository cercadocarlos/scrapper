package com.scrapper.controller;

import com.scrapper.db.MySQLConnectionFactory;
import com.scrapper.modelo.Producto;
import java.util.logging.Logger;

/**
 *
 * @author casa
 */
public class HelperAgentDB extends Thread 
{      
    Producto producto=null;
    MySQLConnectionFactory connectDB=new MySQLConnectionFactory();
    Logger logger = Logger.getLogger(HelperAgentDB.class.getName());

     public HelperAgentDB(Producto prod){
         this.producto=prod;
     }
    public void run() 
    { 
        try
        { 
            // Displaying the thread that is running 
            System.out.println ("Thread " + 
                  Thread.currentThread().getId() + 
                  " is running"); 
             logger.info("Iniciando update en Base de Datos Producto " + producto.getASIN());
            connectDB.updatePrecioProducto(producto);
            connectDB.updatePorProcesar(producto);
            logger.info("Finaliza el gaurdado del Producto" + producto.getASIN());

            
  
        } 
        catch (Exception e) 
        { 
            // Throwing an exception 
            System.out.println ("Exception is caught " ); 
           e.printStackTrace();
        } 
    } 
    
}
