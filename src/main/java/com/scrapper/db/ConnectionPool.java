package com.scrapper.db;

import java.sql.Connection;
import java.sql.SQLException;



import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.sql.Connection;

public class ConnectionPool {
	//private static final Logger LOG = LoggerFactory.getLogger(ConnectionPool.class);
	private static ConnectionPool instance = null;
	private HikariDataSource ds = null;

	static {
		try {
	//		LOG.info("Initializing the connection pool ... ");
			instance = new ConnectionPool();
	//		LOG.info("Connection pool initialized successfully.");
		} catch (Exception e) {
			System.out.println("Exception when trying to initialize the connection pool " + e.getMessage());
			e.printStackTrace();
		}
	}

	private ConnectionPool() {
		HikariConfig config = new HikariConfig("hikariMySQL.properties");
		ds = new HikariDataSource(config);

		// ds.setConnectionTimeout(30000);
		// ds.setIdleTimeout(500);

//		ds.setMinimumIdle(1000);
//		ds.setMaximumPoolSize(10000);

		// ds.setLeakDetectionThreshold(15000);

	}

	public static ConnectionPool getInstance() {
		return instance;
	}

	public Connection getConnection() throws SQLException {
		return ds.getConnection();
	}
}