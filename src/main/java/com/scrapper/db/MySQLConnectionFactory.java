package com.scrapper.db;

import com.scrapper.modelo.Producto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author david
 */
public class MySQLConnectionFactory implements ConnectionDBFactory {

    Logger logger = Logger.getLogger(MySQLConnectionFactory.class.getName());

    @Override
    public boolean updateProductoMLSKU(String sku, String ml) {
        String query = "update productos set mlcode=? where sku=?";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement st2 = conn.prepareStatement(query);) {
            st2.setString(2, sku);
            st2.setString(1, ml);
            st2.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO Auto-generated catch block
            logger.info("Error Modificando Precio Producto " + sku);
            return false;
        }
    }

    @Override
    public boolean updateProductoMLInicial(String asin, String sku, String ml) {
        String query = "update productos set sku=?, mlcode=? where asin=?";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement st2 = conn.prepareStatement(query);) {
            st2.setString(1, sku);
            st2.setString(2, ml);
            st2.setString(3, asin);
            st2.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO Auto-generated catch block
            logger.info("Error Modificando Precio Producto " + asin);
            return false;
        }
    }

    @Override
    public boolean existeProducto(String asin) {
        String query = "select asin from productos where asin=?";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement statement = conn.prepareStatement(query);) {
            statement.setString(1, asin);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String asinProd = rs.getString("asin");
                return true;
            }
            return false;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            logger.info("Error Buscando Producto ");

            return false;
        }
    }

    @Override
    public List<Integer> getConfiguracion() {
        List<Integer> configura = new ArrayList<Integer>();
        String query = "select * from configura";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement statement = conn.prepareStatement(query);) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                int valor1 = rs.getInt("maximoconsulta");
                int valor2 = rs.getInt("productosxminuto");
                int valor3 = rs.getInt("minutosespera");
                int valor4 = rs.getInt("tiempoespera");
                int valor5 = rs.getInt("tiempoupdate");
                int valor6 = rs.getInt("maximoupdate");
                configura.add(valor1);
                configura.add(valor2);
                configura.add(valor3);
                configura.add(valor4);
                configura.add(valor6);
                configura.add(valor5);

            }
            return configura;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.info("Error Buscando Valores de Configuracion ");

            return null;
        }
    }

    @Override
    public boolean agregaVariante(String parentAsin, String asin, String labels, String valores) {
        String query = "INSERT INTO `variantes`(`asinparent`,`asin`, labels,valores,`procesado`) VALUES (?,?,?,?,0)";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement statement = conn.prepareStatement(query);) {
            statement.setString(1, parentAsin);
            statement.setString(2, asin);
            statement.setString(3, labels);
            statement.setString(4, valores);

            statement.execute();
            return true;

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.info("Producto repetido  en VARIANTE" + asin);
        }
        return false;
    }

    @Override
    public boolean noProcesadoASIN(String asin, String motivo) {
        String query = "INSERT INTO noprocesado(asin,motivo) VALUES (?,?)";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement statement = conn.prepareStatement(query);) {
            statement.setString(1, asin);
            statement.setString(2, motivo);
            statement.execute();
            return true;

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.info("Producto repetido " + asin);
            return false;
        }

    }

    @Override
    public boolean noProcesadoVAR(String asin, String motivo) {
        String query = "INSERT INTO noprocesadovar(asin,motivo) VALUES (?,?)";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement statement = conn.prepareStatement(query);) {
            statement.setString(1, asin);
            statement.setString(2, motivo);
            statement.execute();
            return true;

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.info("Producto repetido " + asin);
        }
        return false;

    }

    @Override
    public boolean noExisteASIN(String asin) {
        String query = "INSERT INTO `noexiste`(`asin`) VALUES (?)";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement statement = conn.prepareStatement(query);) {
            statement.setString(1, asin);
            statement.execute();
            return true;

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.info("Producto repetido " + asin);
        }
        return false;

    }

    @Override
    public boolean updatePorProcesar(Producto prod) {
        String query = "update porprocesar set procesado=?  where asin=?";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement st2 = conn.prepareStatement(query);) {
            st2.setBoolean(1, prod.getStatus().equals("ACTIVO") ? true : false);
            st2.setString(2, prod.getASIN());
            st2.executeUpdate();
            return true;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            logger.info("Error Modificando Producto " + prod.getASIN());

            return false;
        }
    }

    @Override
    public boolean updateVariante(String asin) {
        String query = "update variantes set procesado=1  where asin=?";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement st2 = conn.prepareStatement(query);) {
            st2.setString(1, asin);
            st2.executeUpdate();
            return true;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            logger.info("Error Modificando Producto " + asin);

            return false;
        }
    }

    @Override
    public boolean creaProductosAgente(String asin) {
        String query = "INSERT INTO `porprocesar`(`asin`, `procesado`) VALUES (?,0)";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement statement = conn.prepareStatement(query);) {
            statement.setString(1, asin);
            statement.execute();
            return true;

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.info("Producto repetido " + asin);
        }
        return false;

    }

    @Override
    public boolean updatePrecioProducto(Producto prod) {
        String query = "update productos set precio=?, reviews=?, status=?, disponible=?, proveedor=?, precioml=?, statusml=?, shipping=? where asin=?";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement st2 = conn.prepareStatement(query);) {
            st2.setFloat(1, prod.getPrecio());
            st2.setFloat(2, prod.getReviews());
            st2.setString(3, prod.getStatus());
            st2.setString(4, prod.getDisponible());
            st2.setString(5, prod.getProveedor());
            st2.setFloat(6, prod.getPrecioML());
            st2.setString(7, prod.getStatusml());
            st2.setFloat(8, prod.getShipping());

            st2.setString(9, prod.getASIN());

            st2.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO Auto-generated catch block
            logger.info("Error Modificando Precio Producto " + prod.getASIN());
            return false;
        }
    }

    @Override
    public List<String> getProductosxProcesar() {
        List<String> productos = new ArrayList<String>();
        String query = "select asin from productos  order by fechaupdate asc";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement statement = conn.prepareStatement(query);) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String asin = rs.getString("asin");
                productos.add(asin);
            }
            return productos;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            logger.info("Error Buscando Productos a Procesar ");

            return null;
        }
    }

    @Override
    public List<String> getProductosInactivosxProcesar() {
        List<String> productos = new ArrayList<String>();
        String query = "select asin from productos where status='ACTIVO' and statusml='INACTIVO' order by fechaupdate asc";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement statement = conn.prepareStatement(query);) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String asin = rs.getString("asin");
                productos.add(asin);
            }
            return productos;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            logger.info("Error Buscando Productos a Procesar ");

            return null;
        }
    }

    @Override
    public boolean updateProducto(String asin) {
        String query = "update producto_inicial set  procesado=1  where asin=?";
        System.out.println(query + " " + asin);
////        if(url.indexOf("/dp/")>=0){
        //                   String asin=url.substring(url.indexOf("/dp/")+4,url.indexOf("/dp/")+14);
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement st2 = conn.prepareStatement(query)) {
            st2.setString(1, asin);
            st2.executeUpdate();
            return true;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.info("Error Modificando Producto " + asin);

            return false;
        }
        /*}else{
            return false;
        }*/

    }

    @Override
    public List<String> getNuevosProductos(int cantidad) {
        List<String> productos = new ArrayList<String>();
        String query = "select url,asin from producto_inicial where procesado=0 order by fechaupdate desc limit " + cantidad;
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement statement = conn.prepareStatement(query);) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String url = rs.getString("url");
                String asin = rs.getString("asin");
                productos.add(asin);
            }
            return productos;
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO Auto-generated catch block
            logger.info("Error Agregando Nuevos Productos ");

            return null;
        }
    }

    @Override
    public List<String> getProductosVariantes() {
        List<String> productos = new ArrayList<String>();
        String query = "select asin from variantes where procesado=0";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement statement = conn.prepareStatement(query);) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String asin = rs.getString("asin");
                productos.add(asin);
            }
            return productos;
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO Auto-generated catch block
            logger.info("Error Agregando Nuevos Productos ");

            return null;
        }
    }

    @Override
    public boolean creaProductoInit(String asin, String url) {
        String query = "INSERT INTO `producto_inicial`(`asin`, `url`) VALUES (?,?)";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement statement = conn.prepareStatement(query);) {
            statement.setString(1, asin);
            statement.setString(2, url);

            statement.execute();
            return true;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            logger.info("Error Creando Producto_Inicial ");

        }
        return false;

    }

    @Override
    public boolean addProducto(Producto prod) {
        System.out.println(prod);
        String query = "insert into productos (asin, titulo,descripcion,marca,modelo,precio,categoria,subcategoria,reviews,status) values(?,?,?,?,?,?,?,?,?,?)";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement st = conn.prepareStatement(query);) {
            st.setString(1, prod.getASIN());
            st.setString(2, prod.getTitulo());
            st.setString(3, prod.getDescripcion());
            st.setFloat(6, prod.getPrecio());
            st.setString(4, prod.getMarca());
            st.setString(5, prod.getModelo());
            st.setString(7, prod.getCategoria());
            st.setString(8, prod.getSubcategoria());
            st.setFloat(9, prod.getReviews());
            st.setString(10, prod.getStatus());
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.info("Error Creando Producto " + prod.getASIN());
            return false;
        }
    }

    @Override
    public boolean addVariante(Producto prod) {
        System.out.println(prod);
        String query = "insert into variantesdata (asin, titulo,descripcion,marca,modelo,precio,categoria,subcategoria,reviews,status,url) values(?,?,?,?,?,?,?,?,?,?,?)";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement st = conn.prepareStatement(query);) {
            st.setString(1, prod.getASIN());
            st.setString(2, prod.getTitulo());
            st.setString(3, prod.getDescripcion());
            st.setFloat(6, prod.getPrecio());
            st.setString(4, prod.getMarca());
            st.setString(5, prod.getModelo());
            st.setString(7, prod.getCategoria());
            st.setString(8, prod.getSubcategoria());
            st.setFloat(9, prod.getReviews());
            st.setString(10, prod.getStatus());
            st.setString(11, prod.getUrl());

            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.info("Error Creando Producto " + prod.getASIN());
            return false;
        }
    }

    @Override
    public boolean addImagenes(String asin, String url) {
        String query = "insert into imagenes (asin,url) VALUES (?,?)";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement st = conn.prepareStatement(query);) {
            st.setString(1, asin);
            st.setString(2, url);
            st.executeUpdate();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error Creando Imagen " + url + " Producto " + asin);
            return false;
        }
    }

    @Override
    public boolean addBullets(String asin, String url) {
        String query = "insert into bullets (asin,bullet) VALUES (?,?)";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement st = conn.prepareStatement(query);) {
            st.setString(1, asin);
            st.setString(2, url);
            st.executeUpdate();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error Creando Bullet " + url + " Producto " + asin);
            return false;
        }
    }

    @Override
    public boolean addAtributos(String asin, String atributo, String valor) {
        String query = "insert into atributos (asin,atributo,valor) VALUES (?,?,?)";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement st = conn.prepareStatement(query);) {
            st.setString(1, asin);
            st.setString(2, atributo);
            st.setString(3, valor);

            st.executeUpdate();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error Creando Atributos " + atributo + " Producto " + asin);

            return false;
        }
    }

    @Override
    public Producto getProducto(String asin) {
        String query = "select * from productos where asin=?";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement st = conn.prepareStatement(query);) {
            st.setString(1, asin);
            ResultSet rs = st.executeQuery();
            Producto prod = new Producto();
            while (rs.next()) {
                String url = rs.getString("asin");
                float precio = rs.getFloat("precio");
                float review = rs.getFloat("reviews");
                String mlCode = rs.getString("mlcode");
                String status = rs.getString("status");
                float shipping = rs.getFloat("shipping");

                prod.setASIN(asin);
                prod.setPrecio(precio);
                prod.setReviews(review);
                prod.setMlCode(mlCode);
                prod.setStatus(status);
                prod.setShipping(shipping);
                break;
            }
            return prod;
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public boolean guardaHistoricoPrecio(Producto prodOld, Producto prodNew) {
        String query = "insert into historicoprecios "
                + "(asin,precioold,reviewold,precionew,reviewnew,statusnew,disponibilidad,proveedor,"
                + "mlcode,changeML,statusml,preciomlold, preciomlnew,statusold,statusmlold) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement st = conn.prepareStatement(query);) {
            st.setString(1, prodNew.getASIN());
            st.setFloat(2, prodOld.getPrecio());
            st.setFloat(3, prodOld.getReviews());
            st.setFloat(4, prodNew.getPrecio());
            st.setFloat(5, prodNew.getReviews());
            st.setString(6, prodNew.getStatus());
            st.setString(7, prodNew.getDisponible());
            st.setString(8, prodNew.getProveedor());
            st.setString(9, prodNew.getMlCode());
            st.setBoolean(10, prodNew.isChanged());
            st.setString(11, prodNew.getStatusml());
            st.setFloat(12, prodOld.getPrecioML());
            st.setFloat(13, prodNew.getPrecioML());
            st.setString(14, prodOld.getStatus());
            st.setString(15, prodOld.getStatusml());

            st.executeUpdate();
            System.out.println("SE ACTUALIZO " + prodNew.getMlCode());
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("Error Creando Atributos Historico d Precios del Producto " + prodOld.getASIN());

            return false;
        }
    }

    @Override
    public List<String> getProductosUpdate(int cantidad) {
        List<String> productos = new ArrayList<String>();
        String query = "select asin from productos order by fechaupdate asc limit " + cantidad;
        System.out.println("SQL=" + query);
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement statement = conn.prepareStatement(query);) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String asin = rs.getString("asin");
                productos.add(asin);
            }
            System.out.println("Se consiguieron " + productos.size());
            return productos;
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO Auto-generated catch block
            logger.info("Error Buscando Productos para el Update ");

            return null;
        }
    }

    @Override
    public List<String> getProductosUpdateMLInactivos(int cantidad) {
        List<String> productos = new ArrayList<String>();
        String query = "select asin from productos where statusml='INACTIVO' order by fechaupdate asc limit " + cantidad;
        System.out.println("SQL=" + query);
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement statement = conn.prepareStatement(query);) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String asin = rs.getString("asin");
                productos.add(asin);
            }
            System.out.println("Se consiguieron " + productos.size());
            return productos;
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO Auto-generated catch block
            logger.info("Error Buscando Productos para el Update ");

            return null;
        }
    }

    @Override
    public Producto getProductoNew(String asin) {
        String query = "select * from producto_inicial where asin=? order by fechacrea desc";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement st = conn.prepareStatement(query);) {
            st.setString(1, asin);
            ResultSet rs = st.executeQuery();
            Producto prod = new Producto();
            while (rs.next()) {
                String url = rs.getString("asin");
                String precio = rs.getString("titulo");
                String review = rs.getString("descripcion");

                prod.setASIN(asin);
                prod.setTitulo(precio);
                prod.setDescripcion(review);
                break;
            }
            return prod;
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public boolean updateMLProducto(String asin, String mlCode, float precio) {
        String query = "update productos set  precioml=?, mlcode=?,statusml='ACTIVE' where asin=?";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement st2 = conn.prepareStatement(query);) {
            st2.setFloat(1, precio);
            st2.setString(2, mlCode);
            st2.setString(3, asin);

            st2.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO Auto-generated catch block
            logger.info("Error Modificando Precio Producto " + asin);
            return false;
        }
    }

    @Override
    public boolean updateMLData(String asin, String json, String res) {
        String query = "update productos set json=?, respuesta=? where asin=?";
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement st2 = conn.prepareStatement(query);) {
            st2.setString(2, res);
            st2.setString(1, json);
            st2.setString(3, asin);
            st2.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO Auto-generated catch block
            logger.info("Error Modificando Precio Producto " + asin);
            return false;
        }
    }

    @Override
    public List<String> getNuevosProductosVar() {
        String query = "select asinparent from variantesxprocesar where procesado=0 and id>308";
        List<String> productos = new ArrayList<String>();
        try (Connection conn = ConnectionPool.getInstance().getConnection();
                PreparedStatement statement = conn.prepareStatement(query);) {

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String asinProd = rs.getString("asinparent");
                productos.add(asinProd);
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            logger.info("Error Buscando Producto ");

        }
        return productos;
    }

}
