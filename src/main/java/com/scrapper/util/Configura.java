package com.scrapper.util;

import com.scrapper.db.MySQLConnectionFactory;
import java.util.List;
import java.util.logging.Logger;

public class Configura{

    private static Configura instance = null;
    Logger logger = Logger.getLogger(Configura.class.getName());
    MySQLConnectionFactory connectDB=new MySQLConnectionFactory();
   
    public static Configura getInstance() {
        if (Configura.instance == null) {
            Configura.instance = new Configura();
        }

        return Configura.instance;
    }

    private Integer maximoConsulta;

    /**
     * What's the maximum length of content to keep in memory?
     */
    private Integer productosxMinuto;

    /**
     * How should the log files be sorted?
     */
    private Integer minutosEspera;
    
    private Integer tiempoEspera;

    private Integer maximoUpdate;
    private Integer tiempoUpdate;
    
    private Configura() {
       List<Integer> valores=connectDB.getConfiguracion();
       this.maximoConsulta=valores.get(0);
       this.productosxMinuto=valores.get(1);
       this.minutosEspera=valores.get(2);
       this.tiempoEspera=valores.get(3);
        this.maximoUpdate=valores.get(4);
       this.tiempoUpdate=valores.get(5);

    }

    public MySQLConnectionFactory getConnectDB() {
        return connectDB;
    }

    public void setConnectDB(MySQLConnectionFactory connectDB) {
        this.connectDB = connectDB;
    }

    public Integer getMaximoConsulta() {
        return maximoConsulta;
    }

    public void setMaximoConsulta(Integer maximoConsulta) {
        this.maximoConsulta = maximoConsulta;
    }

    public Integer getProductosxMinuto() {
        return productosxMinuto;
    }

    public void setProductosxMinuto(Integer productosxMinuto) {
        this.productosxMinuto = productosxMinuto;
    }

    public Integer getMinutosEspera() {
        return minutosEspera;
    }

    public void setMinutosEspera(Integer minutosEspera) {
        this.minutosEspera = minutosEspera;
    }

    public Integer getTiempoEspera() {
        return tiempoEspera;
    }

    public void setTiempoEspera(Integer tiempoEspera) {
        this.tiempoEspera = tiempoEspera;
    }

    public Integer getMaximoUpdate() {
        return maximoUpdate;
    }

    public void setMaximoUpdate(Integer maximoUpdate) {
        this.maximoUpdate = maximoUpdate;
    }

    public Integer getTiempoUpdate() {
        return tiempoUpdate;
    }

    public void setTiempoUpdate(Integer tiempoUpdate) {
        this.tiempoUpdate = tiempoUpdate;
    }
    
    
}
