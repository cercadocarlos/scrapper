package com.scrapper.util;

/**
 *
 * @author casa
 */
public class ProductoNoExisteException extends Exception{
    
    public ProductoNoExisteException(String msg){
        super(msg);
    }
    
}
